package kz.aitu.oop.practice3;

public class Company {
    public int materialCosts;
    public int employeesNum;

    public Company(int materialCosts, int employeesNum) {
        this.materialCosts = materialCosts;
        this.employeesNum = employeesNum;
    }

    public Company() {
        materialCosts = 200000;
        employeesNum = 3;
    }

    public int getMaterialCosts() {
        return materialCosts;
    }

    public void setMaterialCosts(int materialCosts) {
        this.materialCosts = materialCosts;
    }

    public int getEmployeesNum() {
        return employeesNum;
    }

    public void setEmployeesNum(int employeesNum) {
        this.employeesNum = employeesNum;
    }

    @Override
    public String toString() {
        return super.toString() + " " + "Cost of materials for project: " + materialCosts
                + ". Number of employees in project: " + employeesNum;
    }
}
