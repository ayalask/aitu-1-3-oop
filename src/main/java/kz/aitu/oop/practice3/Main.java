package kz.aitu.oop.practice3;

public class Main {
    public static void main(String[] args) {
        Company company = new Company();
        System.out.println(company.toString());

        int total = 0;

        Company [] companies = new Company[3];
        companies[0] = new Employee(300000);
        companies[1] = new Employee(180000);
        companies[2] = new Employee(100000);

        for (Company companyElement: companies) {
                if (companyElement instanceof Employee) {
                    total += ((Employee) companyElement).getSalary();
                }
        }
        total += company.getMaterialCosts();
        System.out.println("finally: " + total);
    }
}
