package kz.aitu.oop.practice3;

public class Employee extends Company{
    private int salary;

    public Employee(int salary) {
        super();
        this.salary = salary;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return super.toString() + " " + salary;
    }
}

