package kz.aitu.oop.practice2;

public class Main {
    public static void main(String[] args) {
        /*FreightTrain ft = new FreightTrain(500, "brown", 20, 200.0, 500.0);
        ft.transport();
        ft.transport(200.0);

        Locomotive locomotive = new Locomotive("red", 15, 300.0, 1700.0);
        Talgo talgo = new Talgo(1000, "blue", 20, 500.0, 400.0);

        Train[] trains = new Train[5];
        trains[0] = new Locomotive[];

        System.out.println(ft.toString());
        System.out.println(locomotive.toString());
        System.out.println(talgo.toString());*/


        TrainBuilder trainBuilder = new TrainBuilder();

        Train train = trainBuilder
                .addCapacity()
                .addCarriage()
                .addPassengers()
                .assemble();

        System.out.println(train);
    }
}

