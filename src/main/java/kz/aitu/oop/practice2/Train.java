package kz.aitu.oop.practice2;

public class Train {
    private boolean passengers;
    private int capacity;
    private int carriage;

    public Train() {
        passengers = true;
        capacity = 10;
        carriage = 10;
    }

    public boolean getPassengers() {
        return passengers;
    }

    public void setPassengers(boolean passengers) {
        this.passengers = passengers;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getCarriage() {
        return carriage;
    }

    public void setCarriage(int carriage) {
        this.carriage = carriage;
    }

    @Override
    public String toString() {
        return "Train{" +
                "passengers=" + passengers +
                ", capacity=" + capacity +
                ", carriage=" + carriage +
                '}';
    }
}
