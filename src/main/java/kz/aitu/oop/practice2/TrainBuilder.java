package kz.aitu.oop.practice2;

public class TrainBuilder {

    private Train train;

    public TrainBuilder() {
        train = new Train();
    }

    public TrainBuilder addCarriage() {
        train.setCarriage(train.getCarriage() + 1);
        return this;
    }

    public TrainBuilder addCapacity() {
        train.setCapacity(train.getCapacity() + 1);
        return this;
    }

    public TrainBuilder addPassengers() {
        train.setPassengers(true);
        return this;
    }

    public Train assemble() {
        return train;
    }
}
