package kz.aitu.oop.practice2;

public class Talgo extends Train{
    private int passengersNum;

    public Talgo(int passengersNum, String color, int coachCount, double speed, double length) {
        super(color, coachCount, speed, length);
        this.passengersNum = passengersNum;
    }


    public int getPassengersNum() {
        return passengersNum;
    }

    public void setPassengersNum(int passengersNum) {
        this.passengersNum = passengersNum;
    }

    @Override
    public String toString() {
        return super.toString() + " " + "count of coach: "
                + getCoachCount() + ", color of talgo is " + getColor() + ", length of talgo is " + getLength() + " meter"
                + ", speed of train: " + getSpeed();
    }
}
