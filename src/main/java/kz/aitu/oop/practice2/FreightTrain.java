package kz.aitu.oop.practice2;

public class FreightTrain extends Train{
    private double capacity;

    public FreightTrain(double capacity, String color, int coachCount, double speed, double length) {
        super(color, coachCount, speed, length);
        this.capacity = capacity;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public void transport() {
        System.out.println("Freight train transport cargo");
    }

    public void transport(double capacity) {
        System.out.println("Freight train capacity: " + getCapacity());
    }

    @Override
    public int addCoach() {
        return coachCount++;
    }

    @Override
    public String toString() {
        return super.toString() + " " + "count of coach: " + getCoachCount() + ", color of freight train is " + getColor()
                + ", length of train is " + getLength() + " meter"
                + ", speed of train: " + getSpeed();
    }
}

