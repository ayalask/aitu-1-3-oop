package kz.aitu.oop.practice2;

public class Locomotive extends Train {

    public Locomotive(String color, int coachCount, double speed, double length) {
        super(color, coachCount, speed, length);
    }

    @Override
    public String toString() {
        return super.toString() + " " + "count of coach: "
                + getCoachCount() + ", color of locomotive is " + getColor()
                + ", length of train is " + getLength() + " meter" + ", speed of train: " + getSpeed();
    }
}
