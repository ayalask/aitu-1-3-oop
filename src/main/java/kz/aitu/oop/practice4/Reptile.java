package kz.aitu.oop.practice4;

public class Reptile extends Aquarium{
    public String name;
    public int cost;

    public Reptile(String name, int cost) {
        super();
        this.name = name;
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
