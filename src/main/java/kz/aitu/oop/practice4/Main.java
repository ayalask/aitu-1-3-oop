package kz.aitu.oop.practice4;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        int totalPriceOfFish = 0;
        int totalPriceOfReptile = 0;
        /*List<Aquarium> aquariumList = new ArrayList<>();

        for (Aquarium aquariumElement: aquariumList) {
            totalPrice += aquariumElement.getCost();
            System.out.println(totalPrice);
        }*/

        Aquarium[] aquariumElements = new Aquarium[4];
        aquariumElements[0] = new Fish("fish1", 800);
        aquariumElements[1] = new Fish("fish2", 1000);
        aquariumElements[2] = new Reptile("re1", 10000);
        aquariumElements[3] = new Fish("fish3", 2000);

        for (Aquarium aquariumElement: aquariumElements) {
            if (aquariumElement instanceof Fish) {
                totalPriceOfFish += aquariumElement.getCost();
            }

            if (aquariumElement instanceof Reptile) {
                totalPriceOfReptile += aquariumElement.getCost();
            }
            System.out.println(totalPriceOfFish);
            System.out.println(totalPriceOfReptile);
        }
    }
}
