package kz.aitu.oop.practice4;

public class Aquarium {
    public double height;
    public double weight;
    public double cost;

    public Aquarium(double height, double weight) {
        this.height = height;
        this.weight = weight;
        this.cost = cost;
    }

    public Aquarium() {
        height = 5.0;
        weight = 5.0;
        cost = 2000.0;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
