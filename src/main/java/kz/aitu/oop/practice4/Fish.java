package kz.aitu.oop.practice4;

public class Fish extends Aquarium {
    private String name;
    private double cost;


    public Fish(String name, double cost) {
        super();
        this.name = name;
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
