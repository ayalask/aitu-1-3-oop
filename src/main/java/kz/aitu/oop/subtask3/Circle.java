package kz.aitu.oop.subtask3;

public abstract class Circle implements GeometricObject {
    protected double radius;
    protected double pi = 3.14;

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {
        radius = 1.0;
    }

    @Override
    public double getPerimeter() {
        return pi * 2 * radius;
    }

    @Override
    public double getArea() {
        return pi * radius * radius;
    }
}
