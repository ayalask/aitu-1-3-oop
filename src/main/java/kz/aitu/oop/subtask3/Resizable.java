package kz.aitu.oop.subtask3;

public interface Resizable {
    public double resize();
}
