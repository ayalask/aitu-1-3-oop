package kz.aitu.oop.subtask3;

public class ResizableCircle extends Circle implements Resizable {
    private int percent;

    public ResizableCircle(double radius) {
       super(radius);
    }

    @Override
    public double resize() {
        return radius * percent / 100;
    }
}
