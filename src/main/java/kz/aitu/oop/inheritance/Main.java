package kz.aitu.oop.inheritance;

public class Main {
    public static void main(String [] args) {
        Eagle eagle = new Eagle("Aaa", "egg", 5);
        Circle circle = new Circle();
        Rectangle rectangle = new Rectangle();
        //Square square = new Square();

        System.out.println(eagle.toString());
        System.out.println(circle.toString());
        System.out.println(rectangle.toString());
    }
}
