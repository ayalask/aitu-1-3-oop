package kz.aitu.oop.inheritance;

public class Circle extends Shape {

    private double radius;
    private double pi = 3.14;

    public Circle(double radius, String color, boolean filled) {
        super(color, filled);
        this.radius = radius;
    }

    public Circle() {
        radius = 1.0;
    }


    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }


    @Override
    public double getPerimeter() {
        return pi * 2 * radius;
    }

    @Override
    public double getArea() {
        return pi * radius * radius;
    }

    @Override
    public String toString() {
        return super.toString() + " " + radius + " " + getArea() + " " + getPerimeter();
    }
}
