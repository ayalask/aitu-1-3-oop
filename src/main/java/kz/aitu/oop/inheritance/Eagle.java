package kz.aitu.oop.inheritance;

public class Eagle extends Bird {
    private int strength;

    public Eagle(String name, String reproduction, Integer strength) {
        super(name, reproduction);
        this.strength = strength;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    @Override
    public String toString() {
        return super.toString() + " " + strength;
    }
}
