package kz.aitu.oop.inheritance;

public class Bird {
    private String name;
    private String reproduction;

    public Bird(String name, String reproduction) {
        this.name = name;
        this.reproduction = reproduction;
    }

    public void flyUp() {
        System.out.println("Flying up...");
    }

    public void flyDown() {
        System.out.println("Flying down...");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReproduction() {
        return reproduction;
    }

    public void setReproduction(String reproduction) {
        this.reproduction = reproduction;
    }

    public String toString() {
        return name + " " + reproduction;
    }
}
