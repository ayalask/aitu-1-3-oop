package kz.aitu.oop.inheritance;

public class Square extends Rectangle {
    private double side;

    public Square(double side, double width, double length, String color, boolean filled) {
        super(width, length, color, filled);
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public String toString() {
        return super.toString() + " " + side + " " + getArea() + " " + getPerimeter();
    }
}
