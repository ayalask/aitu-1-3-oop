package kz.aitu.oop.examples.practice5;

public class Exception {

    Exception(String arg){
        arg = "test";
        System.out.println(arg);
    }

    public static void main(String[] args) throws java.lang.Exception {
        try {
            System.out.println("try block");
            throw new java.lang.Exception();
        }
        catch (java.lang.Exception e) {
            System.out.println("catch block");
        } finally {
            System.out.println("I am in final block");
        }
    }
}
