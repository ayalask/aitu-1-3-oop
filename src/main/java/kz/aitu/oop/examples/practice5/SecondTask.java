package kz.aitu.oop.examples.practice5;

public class SecondTask {

    public boolean isInt(String s) {
        try {
            Integer.parseInt(s);
            return true;
        }
        catch (java.lang.Exception e) {
            return false;
        }
    }

    public boolean isDouble(String s) {
        try {
            Double.parseDouble(s);
            return true;
        }
        catch (java.lang.Exception e) {
            return false;
        }
    }


}
