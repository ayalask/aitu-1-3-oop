package kz.aitu.oop.examples.assignment3;

public class Main {

    public static void main(String[] args) {
        MyString arr = new MyString();
        MySpecialString arr2 = new MySpecialString();
        NewSpecialString arr3 = new NewSpecialString();
        arr.valueAt(2);
        arr.count(1);
        arr.contains(7);
        arr2.valueAt(1);
        arr2.count(5);
        arr2.contains(3);
        arr3.valueAt(4);
        arr3.contains(0);


        int values [] = {1, 2, 3, 4, 5};
        for (int i = 0; i < values.length; i++) {
            System.out.println(values[i]);
        }
    }
}
