package kz.aitu.oop.examples.assignment3;

public class NewSpecialString {
    public int [] values;
    public int value;
    public int count;
    public int length;
    public int position;
    private boolean containsValue;

    public NewSpecialString() {
        count = 0;
        containsValue = false;
    }

    public int length(){
        length = values.length;
        return length;
    }

    public int valueAt(int position) {
        for (int i = 0; i < length; i++) {
            if (i == position) {
                return values[position];
            }
        }
        return -1;
    }

    public boolean contains(int value) {
        for (int i = 0; i < length; i++) {
            if (value == values[i]) containsValue = true;
            break;
        }
        return containsValue;
    }

    /*public int count(String value) {
            if (value.contains(valu)) {
                count++;
            }
        return count;
    }*/
}

