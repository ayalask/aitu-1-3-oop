package kz.aitu.oop.examples.assignment3;

class MySpecialString {
    public int [] values;
    public int value;
    public int count;
    public int length;
    public int position;
    private boolean containsValue;


    public MySpecialString() {
        count = 0;
        containsValue = false;
    }

    public int length(){
        length = values.length;
        return length;
    }

    public int valueAt(int position) {
        for (int i = 0; i < length; i++) {
            if (i == position) {
                return values[position];
            }
        }
        return -1;
    }

    public boolean contains(int value) {
        for (int i = 0; i < length; i++) {
            if (values[i] == value) containsValue = true;
            break;
        }
        return containsValue;
    }

    public int count(int value) {
        for (int i = 0; i < length; i++) {
            if (values[i] == value) count++;
        }
        return count;
    }

}
