package kz.aitu.oop.examples.practice55;

public class Main {
    public static void main(String[] args) {
        Stone.SemiPreciousStone sps = new Stone.SemiPreciousStone();
        Stone.PreciousStone ps = new Stone.PreciousStone();

        double totalPS = 0, weightPS = 0;
        double totalSPS = 0, weightSPS = 0;

                weightPS += ps.getWeight();
                totalPS += ps.getPrice();

                weightSPS += sps.getWeight();
                totalSPS += sps.getPrice();

            System.out.println("Total price of precious stone: " + totalPS + " KZT" + ", weight: " + weightPS + " carats");
            System.out.println("Total price of semi - precious stone: " + totalSPS + " KZT" + ", weight: " + weightSPS + " carats");
        }

    }
