package kz.aitu.oop.examples.practice55;

public class Stone {
    public int price;
    public double weight;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    static class PreciousStone extends Stone {
        @Override
        public int getPrice() {
            return 20000;
        }

        @Override
        public double getWeight() {
            return 2.0;
        }
    }

    static class SemiPreciousStone extends Stone {
        @Override
        public int getPrice() {
            return 30000;
        }

        @Override
        public double getWeight() {
            return 2.0;
        }
    }
}

