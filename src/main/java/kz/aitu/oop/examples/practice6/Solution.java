package kz.aitu.oop.examples.practice6;

public class Solution {

    public static void main(String[] args) {
        Singleton singleton = Singleton.getSingleInstance();
        singleton.setStr("Hello World!");
        System.out.println(singleton);
    }
}
