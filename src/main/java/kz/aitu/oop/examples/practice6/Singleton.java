package kz.aitu.oop.examples.practice6;
import lombok.Data;

@Data
public class Singleton {
    private static Singleton singleton = null;
    public String str;

    private Singleton() {

    }

    public static Singleton getSingleInstance() {
        if (singleton == null) {
            singleton = new Singleton();
        }
        return singleton;
    }


    @Override
    public String toString() {
        return "Singleton{" +
                "str='" + str + '\'' +
                '}';
    }
}
