package kz.aitu.oop.examples.practice33;

import lombok.Data;

@Data
public class Company {
    private int materials;
    private int materialsCost;
    private int employees;
    private int employeesSalary;

    public Company() {
        materials = 0;
        materialsCost = 0;
        employees = 1;
        employeesSalary = 0;
    }

    @Override
    public String toString() {
        return "Company{" +
                "materials=" + materials +
                ", materialsCost=" + materialsCost +
                ", employees=" + employees +
                ", employeesSalary=" + employeesSalary +
                '}';
    }
}
