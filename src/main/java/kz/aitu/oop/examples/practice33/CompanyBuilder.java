package kz.aitu.oop.examples.practice33;

public class CompanyBuilder {

    private Company company;

    public CompanyBuilder() {
        company = new Company();
    }

    public CompanyBuilder addMaterials() {
        company.setMaterials(company.getMaterials() + 1);
        return this;
    }

    public CompanyBuilder addMaterialsCost() {
        company.setMaterialsCost(company.getMaterialsCost() + 1000);
        return this;
    }

    public CompanyBuilder addEmployees() {
        company.setEmployees(company.getEmployees() + 1);
        return this;
    }

    public CompanyBuilder addEmployeesSalary() {
        company.setEmployeesSalary(company.getEmployeesSalary() + 100000);
        return this;
    }

    public Company createProject() {
        return company;
    }
}
