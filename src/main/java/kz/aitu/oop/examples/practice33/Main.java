package kz.aitu.oop.examples.practice33;

public class Main {
    public static void main(String[] args) {
        CompanyBuilder companyBuilder = new CompanyBuilder();
        int total = 0;
        Company company = companyBuilder
                .addMaterials()
                .addMaterialsCost()
                .addEmployees()
                .addEmployeesSalary()
                .createProject();
        System.out.println(company);
    }
}
