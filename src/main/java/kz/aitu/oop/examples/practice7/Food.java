package kz.aitu.oop.examples.practice7;

public interface Food {
    class Cake implements Food{
        public String getType() {
            return "Someone ordered a Dessert!";
        }
    }

    class Pizza implements Food{
        public String getType() {
            return "Someone ordered a Fast Food!";
        }
    }
}
