package kz.aitu.oop.examples.practice7;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String type = sc.next();
        FoodFactory foodFactory = new FoodFactory();
        Food food = foodFactory.getFood(type);

        System.out.println("The factory returned " + food.getClass());
    }


}
