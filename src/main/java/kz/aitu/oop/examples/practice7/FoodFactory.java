package kz.aitu.oop.examples.practice7;

public class FoodFactory {

    public Food getFood(String type) {
        if (type == "dessert") {
            Food cake = new Food.Cake();
            return cake;
        }
        else {
            Food pizza = new Food.Pizza();
            return pizza;
        }
    }
}
