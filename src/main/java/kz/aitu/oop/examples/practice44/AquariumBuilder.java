package kz.aitu.oop.examples.practice44;

public class AquariumBuilder {
    private Aquarium aquarium;

    public AquariumBuilder() {
        aquarium = new Aquarium();
    }

    public AquariumBuilder addFish() {
        aquarium.setFish(aquarium.getFish() + 1);
        return this;
    }

    public AquariumBuilder addReptile() {
        aquarium.setReptile(aquarium.getReptile() + 1);
        return this;
    }

    public AquariumBuilder addAccessory() {
        aquarium.setAccessory(true);
        return this;
    }

    public Aquarium assemble() {
        return aquarium;
    }
}
