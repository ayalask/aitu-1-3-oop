package kz.aitu.oop.examples.practice44;
public class Main {
    public static void main(String[] args) {
        AquariumBuilder aquariumBuilder = new AquariumBuilder();

        Aquarium aquarium = aquariumBuilder
                .addFish()
                .addReptile()
                .addAccessory()
                .assemble();
        System.out.println(aquarium);

        /*Aquarium aquarium1 = new Aquarium();
        double totalFish = 0, weighFish = 0;

        weighFish += aquarium1.getWeight();

        System.out.println("Total price: " + totalFish + " KZT" + ", weight: " + weighFish + " carats");
        */
    }
}
