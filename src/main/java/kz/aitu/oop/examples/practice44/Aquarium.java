package kz.aitu.oop.examples.practice44;

import lombok.Data;

@Data
public class Aquarium {
    private int fish;
    private int reptile;
    private double weight;
    private boolean accessory;

    public Aquarium() {
        fish = 1;
        reptile = 1;
        weight = 1.5;
        accessory = true;
    }
}
