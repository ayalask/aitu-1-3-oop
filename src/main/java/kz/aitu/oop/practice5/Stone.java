package kz.aitu.oop.practice5;

public class Stone {
    private double weight;
    private double cost;

    public Stone(double weight, double cost) {
        this.weight = weight;
        this.cost = cost;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
