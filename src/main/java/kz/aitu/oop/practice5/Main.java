package kz.aitu.oop.practice5;

public class Main {
    public static void main(String[] args) {
        double totalPS = 0, weightPS = 0;
        double totalSPS = 0, weightSPS = 0;

        Stone[] stones = new Stone[4];
        stones[0] = new PreciousStone(10.1, 200000);
        stones[1] = new SemiPreciousStone(10.2, 300000);
        stones[2] = new PreciousStone(8.4, 180000);
        stones[3] = new SemiPreciousStone(15.11, 600000);

        for (Stone stoneElement: stones) {
            if (stoneElement instanceof PreciousStone) {
                weightPS += stoneElement.getWeight();
                totalPS += stoneElement.getCost();
            }

            if (stoneElement instanceof SemiPreciousStone) {
                weightSPS += stoneElement.getWeight();
                totalSPS += stoneElement.getCost();
            }
            System.out.println("Total price of precious stone: " + totalPS + " KZT" + ", weight: " + weightPS + " carats");
            System.out.println("Total price of semi - precious stone: " + totalSPS + " KZT" + ", weight: " + weightSPS + " carats");
        }
    }
}
