package kz.aitu.oop.repository;

import kz.aitu.oop.entity.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentJDBCRepository {

    private final String url = "jdbc:postgres://localhost:5432/postgres";
    private final String username = "postgres";
    private final String password = "";

    public List<Student> getStudents() {
        List<Student> studentList = new ArrayList<>();

        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * from students");

            while (resultSet.next()) {
                Student student = new Student();
                System.out.println(resultSet.getInt("id"));
                System.out.println(resultSet.getString("name"));
                System.out.println(resultSet.getInt("age"));
                System.out.println(resultSet.getDouble("point"));

                studentList.add(student);
            }
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return studentList;
    }
}
