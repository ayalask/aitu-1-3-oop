package kz.aitu.oop.assignment7;

public class Circle extends Shape {

    protected double radius;
    private double pi = 3.14;

    public Circle(double radius, String color, boolean filled) {
        super(color, filled);
        this.radius = radius;
    }

    public Circle() {
        radius = 1.0;
    }


    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(double radius) {
        double s;
        s = pi * radius * radius;
        return s;
    }

    public double getPerimeter(double radius){
        double perimeter;
        perimeter = pi * 2 * radius;
        return perimeter;
    }

    @Override
    public double getArea() {
        return 0;
    }

    @Override
    public double getPerimeter() {
        return 0;
    }

    @Override
    public String toString() {
        return super.toString() + " " + radius + " " + getArea(1.0) + " " + getPerimeter(1.0);
    }
}
