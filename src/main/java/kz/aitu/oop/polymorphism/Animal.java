package kz.aitu.oop.polymorphism;

public class Animal {
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void eat() {
        System.out.println("Animal " + this.name + " is eating");
    }
}
