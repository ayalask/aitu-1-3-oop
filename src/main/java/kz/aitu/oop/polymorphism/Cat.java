package kz.aitu.oop.polymorphism;

public class Cat extends Animal {

    public Cat(String name) {
        super(name);
    }

    public void jump() {
        System.out.println("Cat " + this.getName() + " is jumping");
    }

    public void jump(int height) {
        System.out.println("Cat " + this.getName() + " jumps " +  height + " meter high");
    }
}
