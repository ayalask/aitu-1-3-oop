package kz.aitu.oop.polymorphism;

public class Fish extends Animal {

    public Fish(String name) {
        super(name);
    }

    public void swim() {
        System.out.println("Fish " + this.getName() + " is swimming");
    }

    public void eat() {
        System.out.println("Fish " + this.getName() + " is eating");
    }
}
