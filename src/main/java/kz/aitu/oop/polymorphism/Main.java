package kz.aitu.oop.polymorphism;

public class Main {
    public static void main(String[] args) {
        Animal animal = new Animal("Animal1");
        Cat cat = new Cat("Cat1");
        cat.jump();
        cat.jump(200);

        Fish fish = new Fish("Fish1");
        fish.swim();
        fish.eat();

        Animal[] animals = new Animal[10];
        animals[0] = new Cat("cat1");
        animals[1] = new Fish("fish1");
        animals[2] = new Cat("cat2");
        animals[3] = new Animal("animal3");
        animals[4] = new Fish("fish3");
        animals[5] = new Cat("cat3");
        animals[6] = new Fish("fish4");
        animals[7] = new Cat("cat4");
        animals[8] = new Cat("cat5");
        animals[9] = new Animal("animal1");

        for (Animal animalElement : animals) {
            animalElement.eat();

            if (animalElement instanceof Cat) {
                cat.jump();
            }

            if (animalElement instanceof Fish) {
                fish.swim();
            }
        }
    }
}
